# colemakprogrammer

Colemak-DH with a custom programmer's layout where all non-alpha keys have been modified.<br>

### Installing
Place `cdhp` in `/usr/share/X11/xkb/symbols`.

### Swap capslock with ctrl/esc
`setxkbmap cdhp -option caps:ctrl_modifier -option lv3:ralt_switch`<br>
<br>
`xcape -e 'Caps_Lock=Escape'`<br>
Xcape can be found in some repos or can be built from [SOURCE](https://github.com/alols/xcape).

<br>image generated with [keyboard-layout-editor](http://www.keyboard-layout-editor.com)
by [ijprest](https://github.com/ijprest/keyboard-layout-editor).
![Preview](layout.png)
<br>If you'd like to tinker with the layout, upload the JSON or copy its content to the layout editor.
